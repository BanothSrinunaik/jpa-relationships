package com.codeforgeyt.jparelationships;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.codeforgeyt.jparelationships.model.oneToOne.bidirectional.Car;
import com.codeforgeyt.jparelationships.model.oneToOne.bidirectional.Owner;
import com.codeforgeyt.jparelationships.model.oneToOne.unidirectional.Address;
import com.codeforgeyt.jparelationships.model.oneToOne.unidirectional.User;
import com.codeforgeyt.jparelationships.repository.oneToOne.bidirectional.CarRepository;
import com.codeforgeyt.jparelationships.repository.oneToOne.bidirectional.OwnerRepository;
import com.codeforgeyt.jparelationships.repository.oneToOne.unidirectional.AddressRepository;
import com.codeforgeyt.jparelationships.repository.oneToOne.unidirectional.UserRepository;

@SpringBootApplication
public class JpaRelationshipsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(JpaRelationshipsApplication.class, args);

//        Beans
      
        UserRepository userRepository = configurableApplicationContext.getBean(UserRepository.class);
        AddressRepository addressRepository = configurableApplicationContext.getBean(AddressRepository.class);
        CarRepository carRepository = configurableApplicationContext.getBean(CarRepository.class);
        OwnerRepository ownerRepository = configurableApplicationContext.getBean(OwnerRepository.class);
        
//        OneToOne Unidirectional
        Address firstAddress = new Address("Hyderabad");
        User firstUser = new User("Srinu Naik");

        addressRepository.save(firstAddress);
        firstUser.setAddress(firstAddress);
        userRepository.save(firstUser);

//        OneToOne bidirectional
        Car car = new Car("m200");
        Owner owner = new Owner("Srinu Naik");

        carRepository.save(car);
        owner.setCar(car);
        ownerRepository.save(owner);
        car.setOwner(owner);
        carRepository.save(car);
        Iterable<Car> cars = carRepository.findAll();
        System.out.println(cars.iterator().next().getOwner().getName());
    }
}
